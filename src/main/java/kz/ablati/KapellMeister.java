package kz.ablati;

import kz.ablati.constants.MathConstants;
import kz.ablati.factory.ServicesInitializerFactory;
import kz.ablati.service.InputDataGeneratorService;
import kz.ablati.service.ModuleDivisorService;
import kz.ablati.service.RecurrenceCalculatorService;
import kz.ablati.service.ResultsOutputService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class KapellMeister {
    private Logger logger = LoggerFactory.getLogger(KapellMeister.class);

    private InputDataGeneratorService inputDataGeneratorService;
    private ModuleDivisorService moduleDivisorService;
    private RecurrenceCalculatorService recurrenceCalculatorService;
    private ResultsOutputService resultsOutputService;

    public KapellMeister(ServicesInitializerFactory servicesInitializerFactory) {
        this.resultsOutputService = servicesInitializerFactory.createResultsOutputService();
        this.inputDataGeneratorService = servicesInitializerFactory.createInputDataGeneratorService();
        this.moduleDivisorService = servicesInitializerFactory.createModuleDivisorService();
        this.recurrenceCalculatorService = servicesInitializerFactory.createRecurrenceCalculatorService(resultsOutputService);

    }

    public void getResults() {
        if (Objects.isNull(MathConstants.SIZE_OF_MASSIVE)){
            logger.error("Количество элементо необходимое суммировать задано не корректно");
            return;
        }
        if (Objects.isNull(MathConstants.NUMBER_FOR_DIVISION_MODULO)){
            logger.error("Делитель указано не корректно");
            return;
        }
        List<BigInteger> recurrenceFormulaNumbersArray = new ArrayList<>();
        inputDataGeneratorService.fillPreliminaryData(recurrenceFormulaNumbersArray);

        if (recurrenceFormulaNumbersArray.isEmpty()){
            logger.error("Входной массив пустой не возможно произвести вычисление");
            return;
        }

        BigInteger sumOfAllArrayAllElements = recurrenceCalculatorService
                .calculateSumOfRecurrenceFormula(recurrenceFormulaNumbersArray);
        resultsOutputService.writeSumOfMassive(sumOfAllArrayAllElements);

        resultsOutputService
                .writeResultOfDividingByModule(moduleDivisorService.
                        divideSumByModulo(sumOfAllArrayAllElements, MathConstants.NUMBER_FOR_DIVISION_MODULO));

    }


}
