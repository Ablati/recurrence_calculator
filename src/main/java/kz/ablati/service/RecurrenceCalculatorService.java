package kz.ablati.service;

import java.math.BigInteger;
import java.util.List;

public interface RecurrenceCalculatorService {


    /**
     * Получение заполненый массив задданный в тз
     *
     * * @param arrayWithPreliminaryData массив заполненный первичными данными {@link List }
     *
     */
    BigInteger calculateSumOfRecurrenceFormula(List<BigInteger> arrayWithPreliminaryData);

}
