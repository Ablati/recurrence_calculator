package kz.ablati.service;

import java.math.BigInteger;
import java.util.List;

public interface ResultsOutputService {


    /**
     * Запись результата;
     *
     *  @param elementIndex  индех элемента
     *  @param elementValue значения элемента
     *
     */
    void writeMassiveElements(Integer elementIndex, BigInteger elementValue);

    /**
     * Запись результата;
     *
     * @param sumOfMassive сумма элементов массива
     *
     */
    void writeSumOfMassive(BigInteger sumOfMassive);


    /**
     * Запись результата;
     *
     * @param dividingByModuleResult результат деления по модулю
     *
     */
    void writeResultOfDividingByModule (BigInteger dividingByModuleResult);

}
