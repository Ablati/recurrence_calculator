package kz.ablati.service;

import java.math.BigInteger;
import java.util.List;

public interface InputDataGeneratorService {

    /**
     * Получение заполненый массив задданный в тз
     *
     * * @param massiveOfFunctionNumbers масив для арефметической операций {@link List }
     *
     */
    void fillPreliminaryData(List<BigInteger> emptyArray);



}
