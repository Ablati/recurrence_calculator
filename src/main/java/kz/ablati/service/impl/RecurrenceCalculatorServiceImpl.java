package kz.ablati.service.impl;

import kz.ablati.KapellMeister;
import kz.ablati.constants.MathConstants;
import kz.ablati.service.RecurrenceCalculatorService;
import kz.ablati.service.ResultsOutputService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

public class RecurrenceCalculatorServiceImpl implements RecurrenceCalculatorService {
    private Logger logger = LoggerFactory.getLogger(RecurrenceCalculatorServiceImpl.class);
    ResultsOutputService resultsOutputService;

    public RecurrenceCalculatorServiceImpl(ResultsOutputService resultsOutputService) {
        this.resultsOutputService = resultsOutputService;
    }

    @Override
    public BigInteger calculateSumOfRecurrenceFormula(List<BigInteger> arrayWithPreliminaryData) {
        if (Objects.isNull(arrayWithPreliminaryData)){
            logger.error("Не возможно высчитать сумму элементов так массив отсутсвует");
            return null;
        }

        BigInteger sumOfMassive = new BigInteger(String.valueOf(0));
        try {
            for (int i = 0; i < MathConstants.SIZE_OF_MASSIVE; i++) {
                arrayWithPreliminaryData.add(arrayWithPreliminaryData.get(i).add(arrayWithPreliminaryData.get(i + 2)));
                resultsOutputService.writeMassiveElements(i, arrayWithPreliminaryData.get(i));
                sumOfMassive = sumOfMassive.add(arrayWithPreliminaryData.get(i));
            }
        } catch (OutOfMemoryError outOfMemoryError) {
            logger.error(" Размер введенного массива {} является слишком большим", MathConstants.SIZE_OF_MASSIVE);
        }catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException){
            logger.error("Размер массива {} меньше чем требуеме количество элементов {}",
                    arrayWithPreliminaryData.size(),
                    MathConstants.SIZE_OF_MASSIVE);
        }
        return sumOfMassive;
    }
}
