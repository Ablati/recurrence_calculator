package kz.ablati.service.impl;

import kz.ablati.KapellMeister;
import kz.ablati.service.InputDataGeneratorService;
import kz.ablati.service.ModuleDivisorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;

public class InputDataGeneratorServiceImpl implements InputDataGeneratorService {
    private Logger logger = LoggerFactory.getLogger(ModuleDivisorService.class);
    @Override
    public void fillPreliminaryData(List<BigInteger> emptyArray) {
        if (Objects.isNull(emptyArray)){
            logger.error("Массив не возможно заполнить, так как массив отсутвует");
            return;
        }
        emptyArray.add(new BigInteger(String.valueOf(1)));
        emptyArray.add(new BigInteger(String.valueOf(2)));
        emptyArray.add(new BigInteger(String.valueOf(3)));
    }

}
