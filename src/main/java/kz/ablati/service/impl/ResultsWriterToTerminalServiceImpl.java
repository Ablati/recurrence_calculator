package kz.ablati.service.impl;

import kz.ablati.KapellMeister;
import kz.ablati.constants.MathConstants;
import kz.ablati.service.ResultsOutputService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Objects;

public class ResultsWriterToTerminalServiceImpl implements ResultsOutputService  {
    private Logger logger = LoggerFactory.getLogger(ResultsWriterToTerminalServiceImpl.class);

    @Override
    public void  writeMassiveElements(Integer elementIndex, BigInteger elementValue) {
        if (Objects.isNull(elementIndex) || Objects.isNull(elementValue)){
            logger.error("Номер элемента {} значение эемента{}", elementIndex, elementValue);
            return;
        }

        System.out.println("Индекс элемента: "
                + elementIndex
                + " Значения элемента: " + elementValue);
    }

    @Override
    public void writeSumOfMassive(BigInteger sumOfMassive) {
        if (Objects.isNull(sumOfMassive)){
            logger.error("Сумма элементов массва не может быть {}", sumOfMassive);
            return;
        }

        System.out.println("Сумма элементов массива: " + sumOfMassive);
    }

    @Override
    public void writeResultOfDividingByModule(BigInteger dividingByModuleResult) {
        if (Objects.isNull(dividingByModuleResult)){
            logger.error("Деление по модулю суммы элементов массва не может быть на {} не может быть {}",
                    MathConstants.NUMBER_FOR_DIVISION_MODULO,
                    dividingByModuleResult);
            return;
        }

        System.out.println("Результат деления суммы элементов массива равен: " + dividingByModuleResult);
    }
}
