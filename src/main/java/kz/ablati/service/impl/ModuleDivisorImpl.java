package kz.ablati.service.impl;

import kz.ablati.KapellMeister;
import kz.ablati.constants.MathConstants;
import kz.ablati.service.ModuleDivisorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.Objects;

public class ModuleDivisorImpl implements ModuleDivisorService {
    private Logger logger = LoggerFactory.getLogger(ModuleDivisorService.class);
    @Override
    public BigInteger divideSumByModulo(BigInteger dividend, BigInteger divisor) {
        if (Objects.isNull(dividend) || Objects.isNull(divisor)){
            logger.error("Делимое чило ={} и делитель = {}",  dividend, divisor);
            return null;
        }

        try {
            return dividend.mod(divisor);
        } catch (ArithmeticException arithmeticException) {
            System.out.println();
            logger.error("Не возможно разделить сумму первых {} элементов на {}",
                    MathConstants.SIZE_OF_MASSIVE, divisor);
        }
        return new BigInteger(String.valueOf(0));
    }
}
