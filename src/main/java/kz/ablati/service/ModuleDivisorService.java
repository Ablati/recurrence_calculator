package kz.ablati.service;

import java.math.BigInteger;
import java.util.List;

public interface ModuleDivisorService {

    /**
     * Получение заполненый массив задданный в тз
     *
     * * @param dividend делимое число
     * * @param divisor делитель
     *
     */
    BigInteger divideSumByModulo(BigInteger dividend, BigInteger divisor);
}
