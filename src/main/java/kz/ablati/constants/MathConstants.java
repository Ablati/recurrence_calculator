package kz.ablati.constants;

import java.math.BigInteger;

public class MathConstants {
    public static final Integer SIZE_OF_MASSIVE = 100;
    public static final BigInteger NUMBER_FOR_DIVISION_MODULO = new BigInteger(String.valueOf(10000));

}
