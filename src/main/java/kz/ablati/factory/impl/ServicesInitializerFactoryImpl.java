package kz.ablati.factory.impl;

import kz.ablati.factory.ServicesInitializerFactory;
import kz.ablati.service.InputDataGeneratorService;
import kz.ablati.service.ModuleDivisorService;
import kz.ablati.service.RecurrenceCalculatorService;
import kz.ablati.service.ResultsOutputService;
import kz.ablati.service.impl.InputDataGeneratorServiceImpl;
import kz.ablati.service.impl.ModuleDivisorImpl;
import kz.ablati.service.impl.RecurrenceCalculatorServiceImpl;
import kz.ablati.service.impl.ResultsWriterToTerminalServiceImpl;

public class ServicesInitializerFactoryImpl implements ServicesInitializerFactory {

    @Override
    public InputDataGeneratorService createInputDataGeneratorService() {
        return new InputDataGeneratorServiceImpl();
    }

    @Override
    public ModuleDivisorService createModuleDivisorService() {
        return new ModuleDivisorImpl();
    }

    @Override
    public RecurrenceCalculatorService createRecurrenceCalculatorService(ResultsOutputService resultsOutputService) {
        return new RecurrenceCalculatorServiceImpl(resultsOutputService);
    }

    @Override
    public ResultsOutputService createResultsOutputService() {
        return new ResultsWriterToTerminalServiceImpl();
    }
}
