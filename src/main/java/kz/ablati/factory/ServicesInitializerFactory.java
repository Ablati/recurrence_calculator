package kz.ablati.factory;

import kz.ablati.service.InputDataGeneratorService;
import kz.ablati.service.ModuleDivisorService;
import kz.ablati.service.RecurrenceCalculatorService;
import kz.ablati.service.ResultsOutputService;

public interface ServicesInitializerFactory {

    InputDataGeneratorService createInputDataGeneratorService();

    ModuleDivisorService createModuleDivisorService();

    RecurrenceCalculatorService createRecurrenceCalculatorService(ResultsOutputService resultsOutputService);

    ResultsOutputService createResultsOutputService();

}
